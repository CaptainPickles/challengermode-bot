import { ElementHandle, Page } from 'puppeteer';

export default class LadderManager {
  protected page: Page;
  protected laddersLink: string[] = []
  public constructor(page: Page) {
    this.page = page;
  }
  public async launchParticipation() {
    await this.page.goto(
      'https://www.challengermode.com/dota2/ladders'
    );
    await this.page.waitForTimeout(4000);
    await this.page.screenshot({ path: 'src/screenshots/check-connection-screenshot.png' });
    await this.fetchAllLadders()

    for (const ladderLink of this.laddersLink) {
      await this.participateToLadder(ladderLink)
    }
  }

  private async fetchAllLadders() {
    const allLaddersDiv = await this.page.$$('a[rel="noopener canonical"]');

    // await allLaddersDiv[8].click()
    for (const [index, value] of allLaddersDiv.entries()) {
      let href = (await value.getProperty("href")).toString()
      if (href.includes("/ladders/")) {
        href = href.replace("JSHandle:", "")
        this.laddersLink.push(href)
      }
    }
    this.removeDuplicatesLinks()
  }
  private removeDuplicatesLinks() {
    this.laddersLink = [...new Set(this.laddersLink)];
    console.log(this.laddersLink)
  }
  private async participateToLadder(ladderLink: string) {
    await this.page.goto(ladderLink)
    await this.page.waitForTimeout(2000);
    const partipateBtn = await this.page.$("#content > div.dis--flx.flx--1-1-auto.flx-dir--col.pos--rel.z--arena-content > div > div > div > div.popup-container--arena-content.p-b--base > div > div:nth-child(1) > div.pos--rel.m-b--medium.m-b--base--mobile.z--999 > div > div > div.dis--flx.flx-gutter--small.jus-con--center.p-t--base.w--100 > div > button")
    if (partipateBtn) {
      await partipateBtn.click()
      await this.page.screenshot({ path: `src/screenshots/lastLink-screenshot.png` });
      // console.log("Success participate to : ", ladderLink)
    }
  }
  // async function participateToLadder() 

  // await allLaddersDiv[0].click()
  //todo selector for parent div ladders #content > div.dis--flx.flx--1-1-auto.flx-dir--col.pos--rel.z--arena-content > div > div > div > div.popup-container--arena-content.p-b--base > div > div > div:nth-child(2) > div > div:nth-child(2) > div > div
}
