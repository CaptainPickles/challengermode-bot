import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import launchParticipation from './LadderManager';
import ora from 'ora';
import chalk from 'chalk';
require('dotenv').config();
import * as cron from 'node-cron';
import challengerConnection from "./challengerConnection"
import LadderManager from './LadderManager';

async function launchBot() {

  puppeteer
    .use(StealthPlugin())
    .launch({
      headless: true,
      args: [
        '--disable-site-isolation-trials',
        '--no-sandbox',
      ], //TODO add proxy exemple : --proxy-server=188.165.59.127:3128
    })
    .then(async (browser) => {
      let challengerPage = await browser.newPage();

      const participatingsSpinner = ora('participanting to all ladders available').start();

      await challengerConnection(challengerPage)
      const ladderManager = new LadderManager(challengerPage)
      await ladderManager.launchParticipation()
      await browser.close();
      participatingsSpinner.succeed("participate to all available ladders")
    });
}

// launch bot every day at 1 hour
let cronJob = cron.schedule('0 0 1 * * *', async () => {
  console.log(
    chalk.blueBright(`running the bot at ${new Date().toLocaleString()}`)
  );
  await launchBot();
}, {
  timezone: 'Europe/Paris'
}, true);