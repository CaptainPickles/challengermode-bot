import { Page } from 'puppeteer';

export default async function challengerConnection(page: Page) {
  await page.goto(
    'https://www.challengermode.com/'
  );
  await page.waitForTimeout(2000);

  await page.click("#mobile-header-react-headroom > div > div > div > div > div.dis--flx.flx--0-0-auto.w--7rem.jus-con--flex-end > div > div > button")
  await page.waitForTimeout(2000);
  await page.type('input[id=Email]', process.env.EMAIL);
  await page.keyboard.press('Enter');
  await page.type('input[id=Password]', process.env.PASSWORD);
  await page.keyboard.press('Enter');
  await page.waitForTimeout(4000);
  // await nextBtn[0].click();
}
